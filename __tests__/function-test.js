import 'react-native';
import React from 'react';
import Home from '../src/Home';
import {shallow} from 'enzyme';

it('function and state', () => {
  let HomeData = jest.fn();
  HomeData.changes(2);
  expect(HomeData.data).toEqual(10);
});
