import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
const Home = () => {
  const [data, setData] = useState();
  const changes = x => {
    return setData(x * 10);
  };
  console.log('halo');
  return (
    <>
      <View
        style={{
          flex: 1,
          backgroundColor: 'gray',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{fontSize: 12, color: 'red'}}>Halo semua </Text>
      </View>
    </>
  );
};

export default Home;
